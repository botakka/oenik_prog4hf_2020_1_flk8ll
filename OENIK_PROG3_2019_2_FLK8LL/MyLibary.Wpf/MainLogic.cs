﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Configuration;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MyLibary.Wpf
{
    class MainLogic
    {
        string url = @"http://localhost:61504/api/BookApi/";
        HttpClient client = new HttpClient();

        void SendMessage(bool success)
        {
            string msg = success ? "Opereation completed succesfully" :
                 "Operation Failed";
            Messenger.Default.Send(msg, "BookResult");
        }
        public List<LibaryViewModel> ApiGetBooks()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<LibaryViewModel>>(json);
            return list;
        }

        public void ApiDelBook(LibaryViewModel book)
        {
            bool success = false;
            if (book != null)
            {
                string json = client.GetStringAsync(url + "del/" + book.ID).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationResult"];
            }
            SendMessage(success);
        }

        bool ApiEditBook(LibaryViewModel book, bool isEditing)
        {
            if (book == null)
            {
                return false;
            }
            string myUrl = isEditing ? url + "mod" : url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing)
            {
                postData.Add(nameof(LibaryViewModel.ID), book.ID.ToString());               
            }
            postData.Add(nameof(LibaryViewModel.Title), book.Title);
            postData.Add(nameof(LibaryViewModel.ISBN), book.ISBN);
            postData.Add(nameof(LibaryViewModel.Publisher), book.Publisher);
            postData.Add(nameof(LibaryViewModel.PublishDate), book.PublishDate);
            string json = client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JObject obj = JObject.Parse(json);

            return (bool)obj["OperationResult"];
        }

        public void EditBook(LibaryViewModel bookVM, Func<LibaryViewModel, bool> editor)
        {
            LibaryViewModel clone = new LibaryViewModel();
            if (clone != null)
            {
                clone.CopyFrom(bookVM);
            }
            bool? sucess = editor?.Invoke(clone);
            if (sucess == true)
            {
                if (bookVM != null)
                {
                   sucess =  ApiEditBook(clone, true);
                }
                else ApiEditBook(clone, false);
            }
            SendMessage(sucess == true);
        }


    }
}
