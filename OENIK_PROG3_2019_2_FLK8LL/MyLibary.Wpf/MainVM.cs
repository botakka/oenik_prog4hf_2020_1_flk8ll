﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MyLibary.Wpf
{
    class MainVM : ViewModelBase
    {
        MainLogic logic;
		private LibaryViewModel selectedBook;
		private ObservableCollection<LibaryViewModel> allBooks;

		public ObservableCollection<LibaryViewModel> AllBooks
		{
			get { return allBooks; }
			set { Set(ref allBooks,value); }
		}


		public LibaryViewModel SelectedBook
		{
			get { return selectedBook; }
			set { Set( ref selectedBook ,value); }
		}


		public ICommand AddCmd { get; set; }
		public ICommand DelCmd { get; set; }
		public ICommand ModCmd { get; set; }
		public ICommand LoadCmd { get; set; }

		public Func<LibaryViewModel,bool> EditorFunc { get; set; }

		public MainVM()
		{
			logic = new MainLogic();

			DelCmd = new RelayCommand(() => logic.ApiDelBook(selectedBook));
			AddCmd = new RelayCommand(() => logic.EditBook(null,EditorFunc));
			ModCmd = new RelayCommand(() => logic.EditBook(null,EditorFunc));
			LoadCmd = new RelayCommand(() => AllBooks = new ObservableCollection<LibaryViewModel> (logic.ApiGetBooks()));

		}

	}
}
