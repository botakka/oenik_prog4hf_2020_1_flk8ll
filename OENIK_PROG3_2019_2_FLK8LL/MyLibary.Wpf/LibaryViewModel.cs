﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MyLibary.Wpf
{
    class LibaryViewModel : ObservableObject
    {
        int id;
        string title;
        string publisher;
        private string isbn;
        string publishDate;

        public string ISBN
        {
            get { return isbn; }
            set { Set(ref isbn, value); }
        }

        public string Publisher
        {
            get { return publisher; }
            set { Set(ref publisher, value); }
        }

        public string PublishDate
        {
            get { return publishDate; }
            set { Set(ref publishDate, value); }
        }



        public int ID
        {
            get { return id; }
            set { Set(ref id, value); }
        }
        public string Title
        {
            get { return title; }
            set { Set(ref title, value); }
        }


        public void CopyFrom(LibaryViewModel otherVM)
        {
            if (otherVM == null)
            {
                return;
            }
            else
            {
                this.ID = otherVM.id;
                this.ISBN = otherVM.isbn;
                this.Title = otherVM.title;
                this.Publisher = otherVM.publisher;
                this.PublishDate = otherVM.publishDate;
            }
        }
    }
}
