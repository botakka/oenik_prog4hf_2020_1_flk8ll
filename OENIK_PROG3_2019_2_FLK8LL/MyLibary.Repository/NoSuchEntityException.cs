﻿// <copyright file="NoSuchBookException.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyLibary.Repository
{
    using System;

    /// <summary>
    /// NoSuchBookException.
    /// </summary>
    public class NoSuchEntityException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NoSuchEntityException"/> class.
        /// NoSuchBookException.
        /// </summary>
        /// <param name="message">exception message</param>
        public NoSuchEntityException(string message)
            : base(message)
        {
        }
    }
}
