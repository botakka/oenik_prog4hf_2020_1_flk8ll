﻿// <copyright file="Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyLibary.Repository
{
    using System;
    using System.Linq;
    using MyLibary.Data;

    /// <summary>
    /// Repo.
    /// </summary>
    public class Repository : ILibaryRepository
    {
        private readonly LibaryDatabaseEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository"/> class.
        /// </summary>
        /// <param name="db">DAtaBase.</param>
        public Repository(LibaryDatabaseEntities db)
        {
            this.db = db;
        }

        /// <inheritdoc/>
        public void AddBook(Konyv book)
        {
            this.db.Konyvs.Add(book);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void AddHires(Kolcsonze hire)
        {
            this.db.Kolcsonzes.Add(hire);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void AddClient(Ugyfel client)
        {
            this.db.Ugyfels.Add(client);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void DeleteBook(int id)
        {
            try
            {
                this.db.Konyvs.Remove(this.GetOne(id));
            }
            catch (NoSuchEntityException ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }

            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void DeleteHire(int id)
        {
            try
            {
                this.db.Kolcsonzes.Remove(this.GetOneHireID(id));
            }
            catch (NoSuchEntityException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <inheritdoc/>
        public void DeleteClient(int id)
        {
            try
            {
                this.db.Ugyfels.Remove(this.GetOneClient(id));
            }
            catch (NoSuchEntityException ex)
            {
                Console.WriteLine(ex.Message);
            }

            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public IQueryable<Konyv> GetAll()
        {
            return this.db.Konyvs;
        }

        /// <inheritdoc/>
        public IQueryable<Ugyfel> GetAllClient()
        {
            return this.db.Ugyfels;
        }

        /// <inheritdoc/>
        public IQueryable<Kolcsonze> GetAllHires()
        {
            return this.db.Kolcsonzes;
        }

        /// <inheritdoc/>
        public Konyv GetOne(int id)
        {
            Konyv book = this.db.Konyvs.Where(x => x.id == id).FirstOrDefault();
            if (book == null)
            {
                throw new NoSuchEntityException("The Book ID: " + id + " not in the repository");
            }

            return book;
        }

        /// <inheritdoc/>
        public Ugyfel GetOneClient(int id)
        {
            Ugyfel client = this.db.Ugyfels.Where(x => x.id == id).FirstOrDefault();
            if (client == null)
            {
                throw new NoSuchEntityException("The client ID: " + id + " not in the repository");
            }

            return client;
        }

        /// <inheritdoc/>
        public Kolcsonze GetOneHireID(int id)
        {
            Kolcsonze rent = this.db.Kolcsonzes.Where(x => x.Kolcsonzo.Equals(this.GetOneClient(id)) && x.Konyv.Equals(this.GetOne(id))).FirstOrDefault();
            if (rent == null)
            {
                throw new NoSuchEntityException("The rental ID: " + id + " not in the repository");
            }

            return rent;
        }

        /// <inheritdoc/>
        public void UpdateBookName(int id, string newName)
        {
            var newIdAndName = this.GetOne(id);
            newIdAndName.Cim = newName;
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateAdress(int id, string newAdress)
        {
            var newAdresses = this.GetOneClient(id);
            newAdresses.Lakcim = newAdress;
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateBook(int id, Konyv book)
        {
            var newBook = this.GetOne(id);
            newBook.Cim = book.Cim;
            newBook.ISBN = book.ISBN;
            newBook.Kiado = book.Kiado;
            newBook.Kiadasi_ev = book.Kiadasi_ev;
            newBook.Keszlet = book.Keszlet;
            newBook.Szabad = book.Szabad;
            this.db.SaveChanges();
        }


        public int LastId()
        {
            return (int)this.db.Konyvs.Max(x => x.id);
        }
    }
}
