﻿// <copyright file="ILibaryRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyLibary.Repository
{
    using MyLibary.Data;

    /// <summary>
    /// ILibaryRepository.
    /// </summary>
    public interface ILibaryRepository : IRepository<Konyv, Ugyfel, Kolcsonze>
    {
        /// <summary>
        /// hello hh.
        /// </summary>
        /// <param name="book">h.</param>
        void AddBook(Konyv book);

        /// <summary>
        /// Adding additional hires.
        /// </summary>
        /// <param name="hire">hires.</param>
        void AddHires(Kolcsonze hire);

        /// <summary>
        /// Adding additional clients.
        /// </summary>
        /// <param name="client">clients.</param>
        void AddClient(Ugyfel client);

        /// <summary>
        /// Delete based on id.
        /// </summary>
        /// <param name="id">delete id.</param>
        void DeleteBook(int id);

        /// <summary>
        /// Delete Hire.
        /// </summary>
        /// <param name="id">id.</param>
        void DeleteHire(int id);

        /// <summary>
        /// Delete Client.
        /// </summary>
        /// <param name="id">id.</param>
        void DeleteClient(int id);

        /// <summary>
        /// Updates.
        /// </summary>
        /// <param name="id">ID:.</param>
        /// <param name="newName">Name:.</param>
        void UpdateBookName(int id, string newName);

        /// <summary>
        /// Updates.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="newAdress">new max count.</param>
        void UpdateAdress(int id, string newAdress);

        /// <summary>
        /// Updates.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="book">the new data.</param>
        void UpdateBook(int id, Konyv book);

        int LastId();
    }
}
