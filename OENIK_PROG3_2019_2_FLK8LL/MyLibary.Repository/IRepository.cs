﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyLibary.Repository
{
    using System.Linq;

    /// <summary>
    /// Irepo.
    /// </summary>
    /// <typeparam name="T">T.</typeparam>
    /// <typeparam name="T1">T1.</typeparam>
    /// <typeparam name="T2">T2.</typeparam>
    public interface IRepository<T, T1, T2>
        where T : class
    {
        /// <summary>
        /// Irepo implementasions.
        /// </summary>
        /// <param name="id">I.</param>
        /// <returns>GetOne.</returns>
        T GetOne(int id);

        /// <summary>
        /// kaki.
        /// </summary>
        /// <returns>kaki</returns>
        int LastId();

        /// <summary>
        /// GetOneClient.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>id:.</returns>
        T1 GetOneClient(int id);

        /// <summary>
        /// GetOne hireId.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>hireids.</returns>
        T2 GetOneHireID(int id);

        /// <summary>
        /// GetAll objects.
        /// </summary>
        /// <returns>Obj.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Gets all the hires from "Konyv" table.
        /// </summary>
        /// <returns>konvy talbe.</returns>

        /// <summary>
        /// Gets all the hires from "Kolcsonzes" table.
        /// </summary>
        /// <returns>Kolcsozes talbe.</returns>
        IQueryable<T2> GetAllHires();

        /// <summary>
        /// Gets all client.
        /// </summary>
        /// <returns>client datas.</returns>
        IQueryable<T1> GetAllClient();
    }
}
