﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BookConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = "http://localhost:61504/api/BookApi/";
            Console.WriteLine("Pleaise wait..");
            Console.ReadLine();            
            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<Book>>(json);

                foreach (var book in list)
                {
                    Console.WriteLine(book);
                }
                Console.ReadLine();

                Dictionary<string, string> postData;
                string response;


                postData = new Dictionary<string, string>();
                postData.Add(nameof(Book.Title), "C# Book");
                postData.Add(nameof(Book.Isbn), "10204567");
                postData.Add(nameof(Book.Publisher), "Reiter Istvan");
                postData.Add(nameof(Book.PublishDate), "1998");


                /*response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData))
               .Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("ADD: "+ response);*/
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();


                int bookId = JsonConvert.DeserializeObject<List<Book>>(json).FirstOrDefault(x =>x.Title.Equals("C# Book")).Id;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(Book.Id), bookId.ToString());
                postData.Add(nameof(Book.Title), "C# Books");
                postData.Add(nameof(Book.Isbn), "10204567");
                postData.Add(nameof(Book.Publisher), "Reiter Istvan");
                postData.Add(nameof(Book.PublishDate), "1889");


                response = client.PostAsync(url + "mod/"+bookId, new FormUrlEncodedContent(postData))
               .Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("MOD: "+response);
                Console.WriteLine("ALL: "+client.GetStringAsync(url+"all").Result);
                Console.ReadLine();


               
                Console.WriteLine("DEL: " + client.GetStringAsync(url + "del/" + bookId).Result);
                Console.WriteLine("ALL: " + client.GetStringAsync(url + "all").Result);
                Console.ReadLine();


            }
        }
    }
}
