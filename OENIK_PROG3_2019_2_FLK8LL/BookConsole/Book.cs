﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookConsole
{
    public class Book
    {
        public int Id { get; set; }

        public string Isbn { get; set; }

        public string Title { get; set; }
        public string Publisher { get; set; }

        public string PublishDate { get; set; }

        public int Supply { get; set; }

        public int FreeBook { get; set; }


        public override string ToString()
        {
            return $"Id={Id}\tISBN ={Isbn}\tTitle = {Title}\tPublisher ={Publisher}";              
        }
    }
}
