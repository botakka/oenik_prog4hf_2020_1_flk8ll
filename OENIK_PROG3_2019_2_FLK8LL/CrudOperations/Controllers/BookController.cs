﻿using AutoMapper;
using CrudOperations.Models;
using MyLibary.Data;
using MyLibary.Logic;
using MyLibary.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebSockets;

namespace CrudOperations.Controllers
{
    public class BookController : Controller
    {
        ILogic logic;
        IMapper mapper;
        BookViewModel vm;

        public BookController()
        {
            LibaryDatabaseEntities lb = new LibaryDatabaseEntities();
            Repository repo = new Repository(lb);
            logic = new LibaryLogic();
            mapper = MapperFactory.CreateMapper();

            vm = new BookViewModel();
            vm.EditedBook = new Book();
            var books = logic.GetAll();
            vm.ListOfBooks = mapper.Map<IList<Konyv>, List<Models.Book>>(books);
        }

        Book GetBook(int id)
        {
            Konyv oneBook = logic.GetOne(id);
            return mapper.Map<Konyv, Models.Book>(oneBook);
        }

        // GET: Book
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("BookIndex", vm);
        }

        // GET: Book/Details/5
        public ActionResult Details(int id)
        {
            return View("BookDetails", GetBook(id));
        }

        public ActionResult Remove(int id)
        {
            TempData["editResult"] = "Delete Fail";
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            vm.EditedBook = GetBook(id);
            return View("BookIndex", vm);
        }

        [HttpPost]
        public ActionResult Edit(Book book, string editAction)
        {
            if (ModelState.IsValid && book != null)
            {
                TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    Konyv k = new Konyv { Cim = book.Title, ISBN = book.Isbn, Kiado = book.Publisher };
                    
                    logic.AddBook(k);
                }
                return RedirectToAction(nameof(Index));
            }
            else
            {
                ViewData["editAction"] = "Edit";
                vm.EditedBook = book;
                return View("BookIndex", vm);

            }
        }

    }
}
