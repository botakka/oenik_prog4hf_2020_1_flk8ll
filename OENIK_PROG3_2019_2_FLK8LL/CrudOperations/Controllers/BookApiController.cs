﻿using AutoMapper;
using CrudOperations.Models;
using MyLibary.Data;
using MyLibary.Logic;
using MyLibary.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Web.Http;

namespace CrudOperations.Controllers
{
    public class BookApiController : ApiController
    {

        public class ApiResult // single point of usage
        {
            public bool OperationResult { get; set; }

        }


        ILogic logic;
        IMapper mapper;
        public BookApiController()
        {
            LibaryDatabaseEntities db = new LibaryDatabaseEntities();
            Repository repo = new Repository(db);
            logic = new LibaryLogic(repo);
            mapper = MapperFactory.CreateMapper();
        }
        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Models.Book> GetAll()
        {
            var books = logic.GetAll();
            return mapper.Map<IList<Konyv>,List<CrudOperations.Models.Book>>(books);
        }

        [ActionName("del")]
        [HttpGet]
        public ApiResult DeleteOneBook(int id)
        {
            var ID = GetAll().Where(x=>x.Id== id).FirstOrDefault();
            logic.DeleteBook(ID.Id);
            return new ApiResult()
            {
                OperationResult = true
            };


        }
        [HttpPost]
        [ActionName("add")]       
        public ApiResult AddOneBook(CrudOperations.Models.Book book)
        {
            logic.AddBook(new Konyv() 
            {
                Cim = book.Title,ISBN = book.Isbn,Kiado = book.Publisher,Kiadasi_ev = book.PublishDate
            });
            return new ApiResult()
            {
                OperationResult = true
            };
        }
        [HttpPost]
        [ActionName("mod")]        
        public ApiResult ModifyOneBook(Book book, int id)
        {
            var ID = GetAll().Where(x => x.Id == id).FirstOrDefault().Id;
            Konyv k = new Konyv()
            {
                Cim = book.Title,
                ISBN = book.Isbn,
                Kiado = book.Publisher,
                Kiadasi_ev = book.PublishDate

            };
            logic.UpdateBook(ID, k);
            return new ApiResult()
            {
                OperationResult = true
            };
        }

    }
}
