﻿using AutoMapper;
using MyLibary.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrudOperations.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(x =>
            {
                x.CreateMap<MyLibary.Data.Konyv, CrudOperations.Models.Book>().
                ForMember(dest => dest.Id, map => map.MapFrom(src => src.id)).
                ForMember(dest => dest.Isbn, map => map.MapFrom(src => src.ISBN)).
                ForMember(dest => dest.Title, map => map.MapFrom(src => src.Cim)).
                ForMember(dest => dest.Publisher, map => map.MapFrom(src => src.Kiado));              
            });
            return config.CreateMapper();
        }
    }
}