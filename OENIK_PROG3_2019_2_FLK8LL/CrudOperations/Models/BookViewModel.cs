﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrudOperations.Models
{
    public class BookViewModel
    {
        public Book EditedBook { get; set; }

        public List<Book> ListOfBooks { get; set; }
    

    }
}