﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CrudOperations.Models
{
    public class Book
    {
        [Display(Name ="ID")]
        [Required]
        public int Id { get; set; }
        [Display(Name = "Isbn")]
        [Required]
        public string Isbn { get; set; }
        [Display(Name = "Title")]
        [Required]
        public string Title { get; set; }
        [Display(Name = "Publisher")]
        [Required]
        public string Publisher { get; set; }

        [Display(Name = "PublishDate")]
        [Required]
        public  string PublishDate { get; set; }

    }
}