﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyLibary.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyLibary.Data;
    using MyLibary.Repository;

    /// <summary>
    /// Ilogic.
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// Adding entities.
        /// </summary>
        /// <param name="book">book.</param>
        void AddBook(Konyv book);

        /// <summary>
        /// adding Hires.
        /// </summary>
        /// <param name="hire">hire.</param>
        void AddHire(Kolcsonze hire);

        /// <summary>
        /// adding clients.
        /// </summary>
        /// <param name="client">clients.</param>
        void AddClient(Ugyfel client);

        /// <summary>
        /// GetOneId.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>BookId.</returns>
        Konyv GetOne(int id);

        /// <summary>
        /// GetOneClientId.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>ClientId.</returns>
        Ugyfel GetOneClient(int id);

        /// <summary>
        /// one Specific id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>hireId.</returns>
        Kolcsonze GetOneHireId(int id);

        /// <summary>
        /// allbooks.
        /// </summary>
        /// <returns>books.</returns>
        IList<Konyv> GetAll();

        /// <summary>
        /// Hires.
        /// </summary>
        /// <returns>hires.</returns>
        IList<Kolcsonze> GetAllHires();

        /// <summary>
        /// clients.
        /// </summary>
        /// <returns>client.</returns>
        IList<Ugyfel> GetAllClient();

        /// <summary>
        /// Updates.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="newName">name.</param>
        void UpdateBookName(int id, string newName);

        /// <summary>
        /// Updates.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="newAdress">name.</param>
        void UpdateAdress(int id, string newAdress);

        /// <summary>
        /// Updates.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="book">name.</param>
        void UpdateBook(int id, Konyv book);

        /// <summary>
        /// deleting book based on id.
        /// </summary>
        /// <param name="id">id.</param>
        void DeleteBook(int id);

        /// <summary>
        /// deleting hire based od id.
        /// </summary>
        /// <param name="id">id.</param>
        void DeleteHire(int id);

        /// <summary>
        /// deleting client based on id.
        /// </summary>
        /// <param name="id">id.</param>
        void DeleteClient(int id);

        /// <summary>
        /// implementing ascending order names of the book authors.
        /// </summary>
        /// <returns>name.</returns>
        IList<Konyv> GetAscendingOrderTitles();

       /// <summary>
       /// returns most book by publishers.
       /// </summary>
       /// <returns>name.</returns>
        IList<Konyv> ContainsSChar();

        /// <summary>
        /// Hires with names.
        /// </summary>
        /// <returns>names.</returns>
        IList<string> ClientNamesWithHires();

        int LastId();
    }
}
