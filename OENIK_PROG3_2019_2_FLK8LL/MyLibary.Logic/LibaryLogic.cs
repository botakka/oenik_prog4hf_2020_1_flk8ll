﻿// <copyright file="LibaryLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyLibary.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using MyLibary.Data;
    using MyLibary.Repository;

    /// <summary>
    /// Libarylogic.
    /// </summary>
    public class LibaryLogic : ILogic
    {
        private readonly ILibaryRepository libaryRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="LibaryLogic"/> class.
        /// Libarylogic.
        /// </summary>
        public LibaryLogic()
        {
            this.libaryRepo = new Repository(new LibaryDatabaseEntities());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LibaryLogic"/> class.
        /// LibaryLogic.constructor.
        /// </summary>
        /// <param name="libaryRepo">lib.</param>
        public LibaryLogic(ILibaryRepository libaryRepo)
        {
            this.libaryRepo = libaryRepo;
        }

        /// <inheritdoc/>
        public void AddBook(Konyv book)
        {
            book.id = LastId() + 1;
            if (string.IsNullOrEmpty(book.Cim))
            {
                throw new ArgumentException("Wrong data");
            }

            this.libaryRepo.AddBook(book);
        }

        /// <inheritdoc/>
        public void AddHire(Kolcsonze hire)
        {
            if (hire.Hosszabbitva == 0 || hire.Megjegyzes == null)
            {
                throw new ArgumentException("Wrong data");
            }

            this.libaryRepo.AddHires(hire);
        }

        /// <inheritdoc/>
        public void AddClient(Ugyfel client)
        {
            if (client.Nev == null || client.Kolcsonzes == null)
            {
                throw new ArgumentException("Wrong data");
            }

            this.libaryRepo.AddClient(client);
        }

        /// <inheritdoc/>
        public IList<Konyv> GetAll()
        {
            return this.libaryRepo.GetAll().ToList();
        }

        /// <inheritdoc/>
        public IList<Ugyfel> GetAllClient()
        {
            return this.libaryRepo.GetAllClient().ToList();
        }

        /// <inheritdoc/>
        public IList<Kolcsonze> GetAllHires()
        {
            return this.libaryRepo.GetAllHires().ToList();
        }

        /// <inheritdoc/>
        public Konyv GetOne(int id)
        {
            return this.libaryRepo.GetOne(id);
        }

        /// <inheritdoc/>
        public Ugyfel GetOneClient(int id)
        {
            return this.libaryRepo.GetOneClient(id);
        }

        /// <inheritdoc/>
        public Kolcsonze GetOneHireId(int id)
        {
            return this.libaryRepo.GetOneHireID(id);
        }

        /// <inheritdoc/>
        public void UpdateAdress(int id, string newAdress)
        {
            this.libaryRepo.UpdateAdress(id, newAdress);
        }

        /// <inheritdoc/>
        public void UpdateBookName(int id, string newName)
        {
            this.libaryRepo.UpdateBookName(id, newName);
        }

        /// <inheritdoc/>
        public void UpdateBook(int id, Konyv book)
        {
            this.libaryRepo.UpdateBook(id, book);
        }

        /// <inheritdoc/>
        public void DeleteClient(int id)
        {
            if (id < 0)
            {
                throw new ArgumentNullException("ID must be positive or null!");
            }

            this.libaryRepo.DeleteClient(id);
        }

        /// <inheritdoc/>
        public void DeleteHire(int id)
        {
            if (id < 0)
            {
                throw new ArgumentNullException("ID must be positive or null!");
            }

            this.libaryRepo.DeleteHire(id);
        }

        /// <inheritdoc/>
        public void DeleteBook(int id)
        {
            if (id < 0)
            {
                throw new ArgumentNullException("ID must be positive or null!");
            }

            this.libaryRepo.DeleteBook(id);
        }

        /// <summary>
        /// returning ascorder of the book titles.
        /// </summary>
        /// <returns>titles.</returns>
        public IList<Konyv> GetAscendingOrderTitles()
        {
            var b = from a in this.libaryRepo.GetAll()
                    orderby a.Cim ascending
                    select a;

            return b.ToList();
        }

        /// <summary>
        /// returning Books with s.
        /// </summary>
        /// <returns>titles.</returns>
        public IList<Konyv> ContainsSChar()
        {
            return (from x in this.libaryRepo.GetAll()
                    where x.Cim.ToUpper().Contains("S")
                    select x).ToList();
        }

        /// <summary>
        /// returns names with hires.
        /// </summary>
        /// <returns>name.</returns>
        public IList<string> ClientNamesWithHires()
        {
            return (from x in this.libaryRepo.GetAll()
                    join y in this.libaryRepo.GetAllClient() on x.id equals y.id
                    select y.Nev).ToList();
        }

        public int LastId()
        {
            return this.libaryRepo.LastId();
        }
    }
}
