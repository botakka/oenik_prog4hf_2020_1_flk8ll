﻿// <copyright file="Menu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyLibary.Program
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using MyLibary.Data;
    using MyLibary.Logic;

    /// <summary>
    /// Menu for Crud and other operations.
    /// </summary>
    public class Menu
    {
        /// <summary>
        /// MainMenu <see cref="Menu"/> class.
        /// </summary>
        /// <param name="logic">business logic.</param>
        public void MainMenu(ILogic logic)
        {
            Console.WriteLine("Welcome in the libary!");
            string password = Console.ReadLine();
            if (password == "pw")
            {
                bool close = true;
                while (close)
                {
                    Console.WriteLine("\nMenu\n" +
                    "1)Get a book\n" +
                    "2)Create book\n" +
                    "3)Retrieve all books\n" +
                    "4)Update book\n" +
                    "5)Delete book\n" +
                    "6)Retrieve all book rentals\n" +
                    "7)Give ascendingorder of the titles\n" +
                    "8)ContainsSChar\n" +
                    "9)Hires and clients name\n" +
                    "10) get from web service\n" +
                    "11)Close\n\n");
                    Console.Write("Choose your option from menu :");
                    if (!int.TryParse(Console.ReadLine(), out int opt))
                    {
                        Console.WriteLine("This is not a number try again");
                        opt = int.MaxValue;
                    }

                    try
                    {
                        switch (opt)
                        {
                            case 1:
                                Console.Write("\nThe ID of the book to be prompted:");
                                int id0;
                                if (!int.TryParse(Console.ReadLine(), out id0))
                                {
                                    Console.WriteLine("This is not a number try again");
                                    break;
                                }

                                Konyv bk = logic.GetOne(id0);
                                this.DisplayBook(bk);
                                break;
                            case 2:
                                logic.AddBook(this.CreateBook());
                                break;
                            case 3:
                                IList<Data.Konyv> konyvek = logic.GetAll();
                                this.DisplayBooks(konyvek);
                                break;
                            case 4:
                                Console.Write("\nThe ID of the book to be modified:");
                                int id1;
                                if (!int.TryParse(Console.ReadLine(), out id1))
                                {
                                    Console.WriteLine("This is not a number try again");
                                    break;
                                }

                                Data.Konyv oldBook;
                                try
                                {
                                    oldBook = logic.GetOne(id1);
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.Message);
                                    break;
                                }

                                Data.Konyv book;
                                if (this.UpdateBook(oldBook, out book))
                                {
                                    logic.UpdateBook(id1, book);
                                }

                                break;
                            case 5:
                                Console.Write("\nThe ID of the book to be deleted:");
                                int id2;
                                if (!int.TryParse(Console.ReadLine(), out id2))
                                {
                                    Console.WriteLine("This is not a number try again");
                                    break;
                                }

                                logic.DeleteBook(id2);
                                break;
                            case 6:
                                IList<Data.Kolcsonze> rentals = logic.GetAllHires();
                                this.DisplayRentals(rentals);
                                break;
                            case 7:
                                IList<Konyv> b = logic.GetAscendingOrderTitles();
                                this.DisplayBooks(b);
                                break;
                            case 8:
                                IList<Konyv> a = logic.ContainsSChar();
                                this.DisplayBooks(a);
                                break;
                            case 9:
                                IList<string> s = logic.ClientNamesWithHires();
                                this.DisplayClients(s);
                                break;
                            case 10:
                                EndPoint ep = new EndPoint();
                                this.DisplayBook(ep.PageLoad());
                                break;
                            case 11:
                                close = false;
                                break;
                            default:
                                break;
                        }
                    }
                    catch (SqlException ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
        }

        /// <summary>
        /// CreateBook<see cref="Menu"/> class.
        /// </summary>
        /// <returns>a newly created book.</returns>
        public Data.Konyv CreateBook()
        {
            int num;
            MyLibary.Data.Konyv book = new MyLibary.Data.Konyv();
            Console.WriteLine("Type the id of the book!");
            while (!int.TryParse(Console.ReadLine(), out num))
            {
                Console.WriteLine("This is not a number try again");
            }

            book.id = num;
            Console.WriteLine("Type the title of the book!");
            do
            {
                book.Cim = Console.ReadLine();
            }
            while (string.IsNullOrEmpty(book.Cim));
            Console.WriteLine("Type the publisher:");
            do
            {
                book.Kiado = Console.ReadLine();
            }
            while (string.IsNullOrEmpty(book.Kiado));

            Console.WriteLine("Type the ISBN of the book:");
            do
            {
                book.ISBN = Console.ReadLine();
            }
            while (string.IsNullOrEmpty(book.ISBN));

            Console.WriteLine("Type the amount of book");
            while (!int.TryParse(Console.ReadLine(), out num))
            {
                Console.WriteLine("This is not a number try again");
            }

            book.Keszlet = num;

            Console.WriteLine("Type the publishing date:");
            do
            {
                book.Kiadasi_ev = Console.ReadLine();
            }
            while (string.IsNullOrEmpty(book.Kiadasi_ev));

            Console.WriteLine("How many books are free ");
            while (!int.TryParse(Console.ReadLine(), out num))
            {
                Console.WriteLine("This is not a number try again");
            }

            book.Szabad = num;
            return book;
        }

        /// <summary>
        /// DisplayBooks <see cref="Menu"/> class.
        /// </summary>
        /// <param name="book">the stored books.</param>
        public void DisplayBook(Data.Konyv book)
        {
             Console.WriteLine();
             Console.WriteLine("\t" + book.id);
             Console.WriteLine("\t" + book.Cim);
             Console.WriteLine("\t" + book.Kiado);
             Console.WriteLine("\t" + book.ISBN);
             Console.WriteLine("\t" + book.Kiadasi_ev);
             Console.WriteLine("\t" + book.Keszlet);
             Console.WriteLine("\t" + book.Szabad);
             Console.WriteLine();
        }

        /// <summary>
        /// DisplayBooks <see cref="Menu"/> class.
        /// </summary>
        /// <param name="books">the stored books.</param>
        public void DisplayBooks(IList<Data.Konyv> books)
        {
            foreach (var item in books)
            {
                this.DisplayBook(item);
            }
        }

        /// <summary>
        /// display method.
        /// </summary>
        /// <param name="clients">disp.</param>
        public void DisplayClients(IList<string> clients)
        {
            foreach (var item in clients)
            {
                Console.WriteLine("\tNames: " + item);
            }
        }

        /// <summary>
        /// DisplayRentals <see cref="Menu"/> class.
        /// </summary>
        /// <param name="rentals">the clients getting books.</param>
        public void DisplayRentals(IList<Data.Kolcsonze> rentals)
        {
            foreach (var item in rentals)
            {
                Console.WriteLine();
                Console.WriteLine("\t" + item.Kolcsonzo);
                Console.WriteLine("\t" + item.Konyv);
                Console.WriteLine("\t" + item.Datum);
                Console.WriteLine("\t" + item.Hosszabbitva);
                Console.WriteLine("\t" + item.Megjegyzes);
                Console.WriteLine();
            }
        }

        /// <summary>
        /// UpdateBook <see cref="Menu"/> class.
        /// </summary>
        /// <param name="oldBook">the stored book.</param>
        /// /// <param name="book">the updated book.</param>
        /// <returns>the update succeeded or not.</returns>
        public bool UpdateBook(Data.Konyv oldBook, out Konyv book)
        {
            book = new Konyv();
            string input;
            Console.WriteLine("Book ID: " + oldBook.id);
            book.id = oldBook.id;
            Console.Write("ISBN: " + oldBook.ISBN + ":");
            input = Console.ReadLine();
            if (!string.IsNullOrEmpty(input))
            {
                book.ISBN = input;
            }
            else
            {
                book.ISBN = oldBook.ISBN;
            }

            Console.Write("Title: " + oldBook.Cim + ":");
            input = Console.ReadLine();
            if (!string.IsNullOrEmpty(input))
            {
                book.Cim = input;
            }
            else
            {
                book.Cim = oldBook.Cim;
            }

            Console.Write("Publisher: " + oldBook.Kiado + ":");
            input = Console.ReadLine();
            if (!string.IsNullOrEmpty(input))
            {
                book.Kiado = input;
            }
            else
            {
                book.Kiado = oldBook.Kiado;
            }

            Console.Write("Publish date: " + oldBook.Kiadasi_ev + ":");
            input = Console.ReadLine();
            if (!string.IsNullOrEmpty(input))
            {
                book.Kiadasi_ev = input;
            }
            else
            {
                book.Kiadasi_ev = oldBook.Kiadasi_ev;
            }

            Console.Write("on the store: " + oldBook.Keszlet + ":");
            input = Console.ReadLine();
            if (!string.IsNullOrEmpty(input))
            {
                if (!long.TryParse(input, out long count))
                {
                    Console.WriteLine("This is not a number try again");
                    return false;
                }

                book.Keszlet = count;
            }
            else
            {
                book.Keszlet = oldBook.Keszlet;
            }

            Console.Write("free to lease: " + oldBook.Szabad + ":");
            input = Console.ReadLine();
            if (!string.IsNullOrEmpty(input))
            {
                if (!long.TryParse(input, out long count))
                {
                    Console.WriteLine("This is not a number try again");
                    return false;
                }

                book.Szabad = count;
            }
            else
            {
                book.Keszlet = oldBook.Keszlet;
            }

            return true;
        }
    }
}
